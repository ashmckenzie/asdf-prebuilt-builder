#!/bin/bash

set -euo pipefail

source "$(cd "$(dirname "$0")" || exit ; /bin/pwd -L)/functions.sh"

add_plugin() {
  local name="${1}"

  if [[ -L "${ASDF_ARCH_PLUGINS_DIR}/${name}" ]]; then
    true
  else
    asdf plugin add ${name}
  fi
}

# -----------------------------------------------------------------------------

ensure_clean_env
ensure_working_directory_opt_gdk

if [[ -d "${ASDF_DIR}" ]]; then
  if [[ -L "${ASDF_DIR}" ]]; then
    if [[ "$(readlink ${ASDF_DIR})" == "${ASDF_ARCH_DIR}" ]]; then
      true
    elif [[ "$(readlink ${ASDF_DIR})" != "${ASDF_ARCH_DIR}" ]]; then
      echo "ERROR: '${ASDF_DIR}' already exists but points to '$(readlink ${ASDF_DIR})'." >&2
      echo -e "INFO: To fix if you're sure, run:\n\nsudo ln -nfs ${ASDF_ARCH_DIR} ${ASDF_DIR}"

      exit 1
    fi
  else
    echo "ERROR: '${ASDF_DIR}' already exists." >&2
    echo -e "INFO: To fix if you're sure, run:\n\nsudo mv ${ASDF_DIR} ${ASDF_DIR}.old ; sudo ln -nfs ${ASDF_ARCH_DIR} ${ASDF_DIR}"

    exit 1
  fi
else
  [[ ! -d "${ASDF_ARCH_DIR}" ]] && git clone https://github.com/asdf-vm/asdf.git ${ASDF_ARCH_DIR} --branch v${ASDF_VERSION}
  echo "INFO: Need sudo in order to create '${ASDF_DIR}' symlink.."
  sudo ln -nfs ${ASDF_ARCH_DIR} ${ASDF_DIR}
fi

source ${ASDF_DIR}/asdf.sh

add_plugin postgres
add_plugin redis
add_plugin ruby

asdf_arch_opts=""

if is_os_darwin; then
  brew bundle --no-lock

  asdf_arch_opts="arch -${ARCH_FORCED}"
fi

MAKELEVEL=0 ${asdf_arch_opts} asdf install
