#!/bin/bash

set -euo pipefail

if [[ -z "${DOCKER_ENV:-}" ]]; then
  echo "ERROR: Need to be inside Docker. Did you forget to run 'make docker-run' first?" >&2
  exit 1
fi

source "$(cd "$(dirname "$0")" || exit ; /bin/pwd -L)/functions.sh"

if is_os_linux; then
  start_clean_linux_build_shell $(arch)
else
  echo "ERROR: Executing environment needs to be Linux."
  exit 1
fi
