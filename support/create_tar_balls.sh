#!/bin/bash

set -euo pipefail

source "$(cd "$(dirname "$0")" || exit ; /bin/pwd -L)/functions.sh"

ensure_clean_env
ensure_working_directory_opt_gdk

ASDF_INSTALLS_DIR="${ASDF_ARCH_DIR}/installs"

all_software_paths=$(find ${ASDF_INSTALLS_DIR} -maxdepth 1 -type d ! -path ${ASDF_INSTALLS_DIR})
packages_path=packages/$(get_platform_tag)/$(get_release_tag)/${ARCH_FORCED}

mkdir -p ${packages_path}

for software_path in ${all_software_paths}; do
  software_name=$(basename ${software_path})
  all_version_paths=$(find ${software_path} -maxdepth 1 -type d ! -path ${software_path})
  software_base_path="${packages_path}/${software_name}"
  software_files_txt_path="${software_base_path}/files.txt"

  mkdir -p "${software_base_path}"
  rm -f "${software_files_txt_path}"

  for version_path in ${all_version_paths}; do
    version=$(basename ${version_path})
    tar_file="${software_base_path}/${software_name}_${version}_$(get_platform_tag)_${ARCH_FORCED}.tar.gz"

    if [[ -f "${tar_file}" ]]; then
      echo "Skipping ${tar_file}.."
    else
      echo "Creating ${tar_file}.."
      tar -cz -C ${ASDF_INSTALLS_DIR}/${software_name}/${version} -f ${tar_file} .
    fi

    echo "$(basename ${tar_file})" >> "${software_files_txt_path}"
  done
done
