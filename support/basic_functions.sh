is_os_darwin() {
	if [[ "${OSTYPE}" == "darwin"* ]]; then
		return 0
	else
		return 1
	fi
}

is_os_linux() {
	if [[ "${OSTYPE}" == "linux"* ]]; then
		return 0
	else
		return 1
	fi
}

is_arch_supported() {
	local arch="${1}"

  if arch -${arch} uptime > /dev/null 2>&1; then
		return 0
	else
		return 1
	fi
}

get_platform_tag() {
	uname | tr '[:upper:]' '[:lower:]'
}

get_release_tag() {
	if [[ "${OSTYPE}" == "linux"* ]]; then
		echo "$(lsb_release -s -c)" | tr A-Z a-z
	elif [[ "${OSTYPE}" == "darwin"* ]]; then
		local system_version=$(defaults read loginwindow SystemVersionStampAsString)

		case ${system_version} in
			12*)
				echo "monterey"
				;;
			11*)
				echo "big_sur"
				;;
			10.15*)
				echo "catalina"
				;;
			*)
				echo "ERROR: Unknown/unsupported platform version." >&2
				exit 1
			;;
		esac
	else
		echo "ERROR: Unknown/unsupported platform." >&2
		exit 1
	fi
}

get_full_platform_tag() {
	echo "$(get_platform_tag)_$(get_release_tag)"
}

ensure_clean_env() {
	if [[ "${CLEAN_ENV:-}" != 1 ]]; then
		echo "ERROR: Not in a clean env. Did you forget to run 'make shell_*' ?" >&2
		exit 1
	fi
}

ensure_working_directory_opt_gdk() {
	# if [[ "$(/bin/pwd -L)" != "/opt/asdf" ]]; then
	# 	echo "ERROR: Working directory must be /opt/asdf." >&2
	# 	exit 1
	# fi
	true
}
